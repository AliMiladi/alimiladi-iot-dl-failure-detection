# alimiladi/iot-dl-failure-detection

* Clone the repo 

## Docker for windows


```batch
cd ali-miladi-iot-dl-failure-detection\docker-for-windows\db-topology\
setup-db-env.bat
live_classify -h
benchmarks -h
```

When the terminal gets closed or to swith to another terminal, please type `setup-aliases.bat` to reset the aliases

To shutdown the topology, type `shutdown-db.bat`

## Docker generic (toolbox/ linux native/ for mac)


```bash
cd ali-miladi-iot-dl-failure-detection/docker-generic/db-topology/
sudo chmod +x shutdown-db setup* 
source ./setup-db-env
live_classify -h
benchmarks -h
```

When the terminal gets closed or to swith to another terminal, please type `source ./setup-aliases` to reset the aliases

To shutdown the topology, type `./shutdown-db`

