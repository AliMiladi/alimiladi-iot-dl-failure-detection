@echo off

echo Setting up aliases ...
echo.
doskey benchmarks=docker run --rm --network dl_db-net -v %cd%:/usr/share/dl-classification-tb18 -it alimiladi/iot-dl-failure-detection benchmarks --dbhostname db $*
doskey live_classify=docker run --rm --network dl_db-net -it alimiladi/iot-dl-failure-detection live_classify --dbhostname db $*
echo DONE